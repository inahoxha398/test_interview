<?php

namespace App\Command;

use App\Sevices\Members;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class GetMembers extends Command
{

    protected static $defaultName = 'app:get-pmembers';
    private $membersService;

    protected function configure(): void
    {
        $this
            ->setHelp('This command allow you to export the parliament members in your db ')
        ;
    }

    public function __construct(Members $membersService)
    {
        $this->membersService = $membersService;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $isExe = $this->membersService->saveEuropeanParliamentMembers();

        if($isExe == true){
            return Command::SUCCESS;
        }
        else{
            return Command::FAILURE;
        }
    }
}