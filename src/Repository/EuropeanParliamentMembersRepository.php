<?php

namespace App\Repository;

use App\Entity\EuropeanParliamentMember;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EuropeanParliamentMember>
 *
 * @method EuropeanParliamentMember|null find($id, $lockMode = null, $lockVersion = null)
 * @method EuropeanParliamentMember|null findOneBy(array $criteria, array $orderBy = null)
 * @method EuropeanParliamentMember[]    findAll()
 * @method EuropeanParliamentMember[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EuropeanParliamentMembersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EuropeanParliamentMember::class);
    }

    public function add(EuropeanParliamentMember $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(EuropeanParliamentMember $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return EuropeanParliamentMember[] Returns an array of EuropeanParliamentMember objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EuropeanParliamentMember
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
