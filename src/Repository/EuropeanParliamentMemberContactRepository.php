<?php

namespace App\Repository;

use App\Entity\EuropeanParliamentMemberContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EuropeanParliamentMemberContact>
 *
 * @method EuropeanParliamentMemberContact|null find($id, $lockMode = null, $lockVersion = null)
 * @method EuropeanParliamentMemberContact|null findOneBy(array $criteria, array $orderBy = null)
 * @method EuropeanParliamentMemberContact[]    findAll()
 * @method EuropeanParliamentMemberContact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EuropeanParliamentMemberContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EuropeanParliamentMemberContact::class);
    }

    public function add(EuropeanParliamentMemberContact $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(EuropeanParliamentMemberContact $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return EuropeanParliamentMemberContact[] Returns an array of EuropeanParliamentMemberContact objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EuropeanParliamentMemberContact
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
