<?php

namespace App\Sevices;

use App\Entity\EuropeanParliamentMember;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;


class Members
{
    protected $em;

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }

   public function saveEuropeanParliamentMembers(){

        $europeanParliamentMember = new EuropeanParliamentMember();
        $xmldata = simplexml_load_file("https://www.europarl.europa.eu/meps/en/full-list/xml/a") or die("Failed to load");
        foreach($xmldata->children() as $member) {
            $europeanParliamentMember->setCountry($member['country']);
            $europeanParliamentMember->setFullName($member['fullName']);
            $europeanParliamentMember->setMemberId($member['id']);
            $europeanParliamentMember->setNationalPoliticalGroup($member['nationalPoliticalGroup']);
            $europeanParliamentMember->setPoliticalGroup($member['politicalGroup']);
            $isContactSaved = $this->saveEuropeanParliamentMembersContacts($member['fullName'], $member['id']);

            try {
                $this->em->persist($europeanParliamentMember);
            }catch (Exception $exception){
                return false;
            }

        }
        $this->em->flush();
        return true;
   }

    public function saveEuropeanParliamentMembersContacts(StringType $memberName, IntegerType $memberId){
        $urlOfMember = "https://www.europarl.europa.eu/meps/en/".$memberId."/".$memberName."/home";
        $html = file_get_contents($urlOfMember);
        $crawler = new Crawler($html);
        //get elements
        //build the object and save in db

    }

}