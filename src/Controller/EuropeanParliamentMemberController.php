<?php

namespace App\Controller;

use App\Entity\EuropeanParliamentMember;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class EuropeanParliamentMemberController extends AbstractController
{

    /**
     * @Route("/members/list", name="member_list", methods={"GET"})
     */
    public function showMembersInformation(): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $membersArray = array();
        $membersObject = $em->getRepository(EuropeanParliamentMember::class)->findAll();
        foreach ($membersObject as $member){
            $item = [
                'id' => $member->getMemberId(),
                'full_name' => $member->getFullName(),
                'country' => $member->getCountry(),
                'national_political_group' => $member->getNationalPoliticalGroup(),
                'political_group' => $member->getPoliticalGroup()
            ];
            $membersArray[] = $item;
        }

        return $this->json($membersArray);
    }

    /**
     * @Route("/contact/{id}", name="member_contact", methods={"GET"})
     */
    public function getContactsByMemberId(int $id): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $data = array();
        $contacts = $em->getRepository(EuropeanParliamentMember::class)->getEuropeanParliamentMemberId($id);
        foreach ($contacts as $contact){
            $item = [
                'address' => $contact->getAddress(),
                'telephone_no' =>$contact->getTelephoneNumber(),
                'fax_no' => $contact->getFaxNo(),
                'email' => $contact->getEmail(),
                'website' => $contact->getWebsite()
            ];
            $data[] = $item;
        }

        return $this->json($data);
    }


}

