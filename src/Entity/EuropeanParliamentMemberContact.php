<?php

namespace App\Entity;

use App\Repository\EuropeanParliamentMemberContactRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EuropeanParliamentMemberContactRepository::class)
 */
class EuropeanParliamentMemberContact
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $telephoneNumber;

    /**
     * @ORM\Column(type="integer")
     */
    private $faxNo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $social = [];


    /**
     * @ORM\ManyToOne(targetEntity=EuropeanParliamentMember::class, inversedBy="europeanParliamentMemberId")
     * @ORM\JoinColumn(nullable=false)
     */
    private $europeanParliamentMember;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getTelephoneNumber(): ?int
    {
        return $this->telephoneNumber;
    }

    public function setTelephoneNumber(int $telephoneNumber): self
    {
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }

    public function getFaxNo(): ?int
    {
        return $this->faxNo;
    }

    public function setFaxNo(int $faxNo): self
    {
        $this->faxNo = $faxNo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getSocial(): ?array
    {
        return $this->social;
    }

    public function setSocial(?array $social): self
    {
        $this->social = $social;

        return $this;
    }


    public function getEuropeanParliamentMember(): ?EuropeanParliamentMember
    {
        return $this->europeanParliamentMember;
    }

    public function setEuropeanParliamentMember(?EuropeanParliamentMember $europeanParliamentMember): self
    {
        $this->europeanParliamentMember = $europeanParliamentMember;

        return $this;
    }
}
