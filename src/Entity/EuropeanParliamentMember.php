<?php

namespace App\Entity;

use App\Repository\EuropeanParliamentMembersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EuropeanParliamentMembersRepository::class)
 */
class EuropeanParliamentMember
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $fullName;

    /**
     * @ORM\Column(type="int")
     */
    private $memberId;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $nationalPoliticalGroup;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $politicalGroup;

    /**
     * @ORM\OneToMany(targetEntity=EuropeanParliamentMemberContact::class, mappedBy="europeanParliamentMember", orphanRemoval=true)
     */
    private $europeanParliamentMemberId;

    public function __construct()
    {
        $this->europeanParliamentMemberId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getNationalPoliticalGroup(): ?string
    {
        return $this->nationalPoliticalGroup;
    }

    public function setNationalPoliticalGroup(string $nationalPoliticalGroup): self
    {
        $this->nationalPoliticalGroup = $nationalPoliticalGroup;

        return $this;
    }

    public function getPoliticalGroup(): ?string
    {
        return $this->politicalGroup;
    }

    public function setPoliticalGroup(string $politicalGroup): self
    {
        $this->politicalGroup = $politicalGroup;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * @param mixed $memberId
     */
    public function setMemberId($memberId): void
    {
        $this->memberId = $memberId;
    }

    /**
     * @return Collection<int, EuropeanParliamentMemberContact>
     */
    public function getEuropeanParliamentMemberId(): Collection
    {
        return $this->europeanParliamentMemberId;
    }

    public function addEuropeanParliamentMemberId(EuropeanParliamentMemberContact $europeanParliamentMemberId): self
    {
        if (!$this->europeanParliamentMemberId->contains($europeanParliamentMemberId)) {
            $this->europeanParliamentMemberId[] = $europeanParliamentMemberId;
            $europeanParliamentMemberId->setEuropeanParliamentMember($this);
        }

        return $this;
    }

    public function removeEuropeanParliamentMemberId(EuropeanParliamentMemberContact $europeanParliamentMemberId): self
    {
        if ($this->europeanParliamentMemberId->removeElement($europeanParliamentMemberId)) {
            // set the owning side to null (unless already changed)
            if ($europeanParliamentMemberId->getEuropeanParliamentMember() === $this) {
                $europeanParliamentMemberId->setEuropeanParliamentMember(null);
            }
        }

        return $this;
    }

}
